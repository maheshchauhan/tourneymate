import 'package:flutter/material.dart';
import 'package:tourneymate/screens/record_score.dart';

class MatchTile extends StatefulWidget {

  final String tournamentId;
  final String matchId;
  final String teamA;
  final int teamAScore;
  final String teamB;
  final int teamBScore;

  const MatchTile({Key key, this.teamA, this.teamAScore, this.teamB, this.teamBScore, this.tournamentId, this.matchId}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _MatchTileState();
  }
}

class _MatchTileState extends State<MatchTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 5.0),
      child: Container(
        color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              children: <Widget>[
                Image.asset(
                  "assets/foosball.png",
                  width: 40,
                  height: 40,
                ),
                SizedBox(width: 10),
                Column(
                    children: <Widget>[
                      Text(widget.teamA, style: TextStyle(fontSize: 20.0, color: Colors.black)),
                      SizedBox(height: 10),
                      Text(widget.teamB, style: TextStyle(fontSize: 20.0, color: Colors.black)),
                    ]
                ),
                Spacer(),
                showScore(widget.teamAScore, widget.teamBScore),
              ],
            )
          )
      )
    );
  }

  Widget showScore(int teamAScore, int teamBScore) {
    return teamAScore >=0 ? Column(
        children: <Widget>[
          Text("$teamAScore", style: TextStyle(fontSize: 20.0, color: Colors.deepOrange)),
          SizedBox(height: 10),
          Text("$teamBScore", style: TextStyle(fontSize: 20.0, color: Colors.deepOrange)),
        ]
    ) : RaisedButton(
      child: const Text('Record Score'),
      textColor: Colors.black,
      onPressed: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) => RecordScore(teamA: widget.teamA, teamB: widget.teamB, tournamentId: widget.tournamentId, matchId: widget.matchId,)));
      },
    );
  }
}
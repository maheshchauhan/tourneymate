import 'package:flutter/material.dart';
import 'package:tourneymate/widgets/models/userinfo.dart';

class TournamentCard extends StatelessWidget {
  final String title;

  final String description;

  final String titleImage;

  final int playersCount;

  // final List<UserInfo> users;

  const TournamentCard(
      {Key key,
      this.title,
      this.description,
      this.titleImage,
      this.playersCount = 0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      height: 150,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Theme.of(context).cardColor,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Expanded(flex: 1, child: _buildImage()),
          Expanded(flex: 2, child: _buildInfoCard(context))
        ],
      ),
    );
  }

  Widget _buildImage() {
    return Container(
      decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.white),
      padding: EdgeInsets.all(25),
      child: Image.asset(
        "assets/tournament_icon.png",
      ),
    );
  }

  Widget _buildInfoCard(BuildContext context) {
    return Container(
      // width: 200,
      padding: EdgeInsets.only(left: 15),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(this.title, style: Theme.of(context).textTheme.caption),
          Text(
            this.description,
            style: Theme.of(context).textTheme.subhead,
            overflow: TextOverflow.clip,
          ),
          Text(
            'Total players ${this.playersCount.toString()}',
            style: Theme.of(context).textTheme.subtitle,
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';

class StandingTile extends StatelessWidget {
  final String title;
  final String titleImage;
  final int playersCount;

  // final List<UserInfo> users;

  const StandingTile(
      {Key key, this.title, this.titleImage, this.playersCount = 0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        child: Container(
          padding: EdgeInsets.all(10),
          height: 100,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(25),
              color: Theme.of(context).primaryColor,
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.black,
                    offset: Offset(0.5, 0.2),
                    blurRadius: 40.0)
              ]),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(flex: 1, child: _buildImage()),
              Expanded(flex: 2, child: _buildInfoCard(context))
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildImage() {
    return Container(
      decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.white),
      padding: EdgeInsets.all(12),
      child: Image.asset(
        "assets/tournament_icon.png",
      ),
    );
  }

  Widget _buildInfoCard(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(this.title, style: Theme.of(context).textTheme.title),
        ],
      ),
    );
  }
}

class UserInfo {
  final String userName;
  final String userProfileImage;
  UserInfo(this.userName, this.userProfileImage);
}

import 'package:flutter/material.dart';
import 'package:tourneymate/data/tournaments.dart';

class TournamentDetails extends StatelessWidget {
  const TournamentDetails({Key key, Tournament tournament}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text("tournament details"),
      ),
    );
  }
}

class TournamentDetailsPageRoute extends MaterialPageRoute {
  TournamentDetailsPageRoute(Tournament tournament)
      : super(
            builder: (context) => new TournamentDetails(
                  key: Key(tournament.title),
                  tournament: tournament,
                ));
  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    return FadeTransition(opacity: animation, child: child);
  }
}

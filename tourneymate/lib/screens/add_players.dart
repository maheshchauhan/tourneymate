import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:tourneymate/data/new_tournament_details.dart';

class AddPlayers extends StatefulWidget {
  final NewTournamentDetails newTournamentDetails;
  AddPlayers({Key key, this.newTournamentDetails}) : super(key: key);

  _AddPlayersState createState() => _AddPlayersState();
}

class _AddPlayersState extends State<AddPlayers> {
  List<PlayerInfoInput> players = List();
  @override
  void initState() {
    super.initState();
    print("about to fetch");
    Firestore.instance
        .collection("players")
        .getDocuments()
        .then((querySnapshot) {
      print("documents are");
      setState(() {
        querySnapshot.documents
            .map((documentSnapshot) => {print(documentSnapshot.data)});

        querySnapshot.documents.forEach((document) {
          players.add(PlayerInfoInput(
              id: document.data["name"],
              name: document.data["name"],
              isSelected: false));
        });
      });

      //print(players);

      //   // docs.forEach((doc) => {
      //   //   doc.documents.first.
      //   // });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add players"),
      ),
      body: _buildForm(context),
    );
  }

  Widget _buildForm(BuildContext contextt) {
    return Container(
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.only(top: 30, bottom: 10, left: 20, right: 20),
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        border: Border.all(width: 1, color: Theme.of(context).primaryColor),
      ),
      child: Center(
        child: Column(
          children: <Widget>[
            _buildPlayerCheckbox(players),
            // StreamBuilder<QuerySnapshot>(
            //   stream: Firestore.instance.collection('players').snapshots(),
            //   builder: (BuildContext context,
            //       AsyncSnapshot<QuerySnapshot> snapshot) {
            //     if (snapshot.hasError)
            //       return new Text('Error: ${snapshot.error}');
            //     switch (snapshot.connectionState) {
            //       case ConnectionState.waiting:
            //         return new Text('Loading...');
            //       default:
            //         return new ListView(
            //           shrinkWrap: true,
            //           scrollDirection: Axis.vertical,
            //           children: snapshot.data.documents
            //               .map((DocumentSnapshot document) {
            //             return CheckboxListTile(
            //               value: false,
            //               title: Text(document.data["name"]),
            //               onChanged: null,
            //             );
            //           }).toList(),
            //         );
            //     }
            //   },
            // ),
            Padding(
              padding: EdgeInsets.only(top: 20),
              child: RaisedButton(
                onPressed: () {
                  onSave();
                },
                elevation: 2,
                child: Text("Create tournament"),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildPlayerCheckbox(List<PlayerInfoInput> players) {
    return players.length > 0
        ? ListView(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            children: players.map((doc) {
              return CheckboxListTile(
                value: doc.isSelected,
                title: Text(doc.name),
                onChanged: ((value) {
                  print("${doc.name} is $value");
                  setState(() {
                    doc.isSelected = value;
                  });
                }),
              );
            }).toList())
        : Text("waiting");
  }

  void onSave() {
    List<PlayerInfoInput> selectedPlayer = this.players.where((player) {
      return player.isSelected == true;
    }).toList();

    if (selectedPlayer.length > 0) {
      var teams = buildTeams(selectedPlayer);
      var matches = buildMatches(teams);
      TournamentInfo tournamentInfo = TournamentInfo(
          name: this.widget.newTournamentDetails.name,
          description: this.widget.newTournamentDetails.description,
          playersCount: selectedPlayer.length,
          teams: teams,
          matches: matches);
      var map = getMapFromObject(tournamentInfo);

      Firestore.instance.collection('tournaments').document().setData(map);
      print(matches);
    }
  }

  List<MatchInfo> buildMatches(List<TeamInfo> teams) {
    var matches = List<MatchInfo>();
    for (int i = 0; i < teams.length; i++) {
      var firstTeam = teams[i];
      for (int j = i + 1; j < teams.length; j++) {
        var secondTeam = teams[j];
        var match =
            MatchInfo(matchId: "${i.toString()}_${j.toString()}", teams: []);
        match.teams.add(MatchTeamInfo(teamId: firstTeam.id));
        match.teams.add(MatchTeamInfo(teamId: secondTeam.id));
        matches.add(match);
      }
    }

    return matches;
  }

  List<TeamInfo> buildTeams(List<PlayerInfoInput> players) {
    List<PlayerInfoInput> newList = players.toList();
    List<TeamInfo> teams = List<TeamInfo>();
    int teamCount = (players.length / 2).ceil();
    int playersInTeamCount = 2;

    for (int idx = 0; idx < teamCount; idx++) {
      TeamInfo team = TeamInfo(
          id: "team_${idx.toString()}",
          name: "Team ${idx.toString()}",
          players: []);
      for (int playerNo = 0; playerNo < playersInTeamCount; playerNo++) {
        PlayerInfoInput player = pickPlayers(newList);
        if (player != null) {
          team.players.add(player.id);
        }
      }
      teams.add(team);
    }

    return teams;
  }

  PlayerInfoInput pickPlayers(List<PlayerInfoInput> players) {
    if (players.length == 0) {
      return null;
    }

    int rand = new Random().nextInt(players.length);
    return players.removeAt(rand);
  }
}

Map<String, dynamic> getMapFromObject(TournamentInfo tournament) {
  Map<String, dynamic> mp = Map<String, dynamic>();
  mp["name"] = tournament.name;
  mp["description"] = tournament.description;
  mp["playersCount"] = tournament.playersCount;
  mp["teams"] = List<Map<String, dynamic>>();
  tournament.teams.forEach((team) {
    var teamMap = Map<String, dynamic>();
    teamMap["id"] = team.id;
    teamMap["name"] = team.name;
    teamMap["players"] = team.players;
    mp["teams"].add(teamMap);
  });

  mp["matches"] = List<Map<String, dynamic>>();
  tournament.matches.forEach((match) {
    var matchMap = Map<String, dynamic>();
    matchMap["matchId"] = match.matchId;
    matchMap["teams"] = List<Map>();
    match.teams.forEach((matchTeam) {
      var matchTeamMap = Map<String, dynamic>();
      matchTeamMap["score"] = matchTeam.score;
      matchTeamMap["teamId"] = matchTeam.teamId;
      matchMap["teams"].add(matchTeamMap);
    });
    mp["matches"].add(matchMap);
  });

  return mp;
}

class PlayerInfoInput {
  bool isSelected;
  final String id;
  final String name;
  PlayerInfoInput({this.id, this.name, this.isSelected});
}

class TeamInfo {
  final String id;
  final String name;
  List<String> players = List<String>();

  TeamInfo({this.id, this.name, this.players});
}

class MatchTeamInfo {
  int score;
  String teamId;
  MatchTeamInfo({this.score = -1, this.teamId});
}

class MatchInfo {
  String matchId;
  List<MatchTeamInfo> teams;
  MatchInfo({this.matchId, this.teams});
}

class TournamentInfo {
  String description;
  String name;
  int playersCount;
  List<MatchInfo> matches;
  List<TeamInfo> teams;
  TournamentInfo(
      {this.name,
      this.description,
      this.playersCount,
      this.matches,
      this.teams});
}

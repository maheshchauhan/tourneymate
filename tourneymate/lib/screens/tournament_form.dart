import 'package:flutter/material.dart';
import 'package:tourneymate/data/new_tournament_details.dart';
import 'package:tourneymate/screens/add_players.dart';

class TournamentForm extends StatefulWidget {
  TournamentForm() : super();

  _TournamentFormState createState() => _TournamentFormState();
}

class _TournamentFormState extends State<TournamentForm> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  bool _autoValidate = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("New tournament"),
      ),
      body: _buildForm(),
    );
  }

  Widget _buildForm() {
    return Form(
      key: _formKey,
      autovalidate: _autoValidate,
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Center(
          child: Container(
            height: 250,
            padding: EdgeInsets.only(top: 30, bottom: 10, left: 20, right: 20),
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
              border:
                  Border.all(width: 1, color: Theme.of(context).primaryColor),
            ),
            child: Column(
              children: <Widget>[
                TextFormField(
                  controller: nameController,
                  validator: emptyValidator,
                  decoration: InputDecoration(labelText: "Enter name"),
                ),
                TextFormField(
                  controller: descriptionController,
                  decoration: InputDecoration(labelText: "Enter description"),
                  validator: emptyValidator,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: RaisedButton(
                    onPressed: () {
                      this.onSave();
                    },
                    elevation: 2,
                    child: Text("Add players"),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void onSave() {
    if (this._formKey.currentState.validate()) {
      print(
          "Name : ${nameController.text} ; Description: ${descriptionController.text}");
      NewTournamentDetails newTournament = NewTournamentDetails(
          name: nameController.text, description: descriptionController.text);
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => AddPlayers(
                    newTournamentDetails: newTournament,
                  )));
    } else {
      setState(() {
        _autoValidate = true;
      });
    }
  }
}

String emptyValidator(String value) {
  if (value.isEmpty) {
    return "Enter the description";
  }
  return null;
}

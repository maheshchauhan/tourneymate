import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:tourneymate/screens/matches.dart';
import 'package:tourneymate/screens/tournament_form.dart';
import 'package:tourneymate/widgets/tournament_card.dart';

class TournamentList extends StatefulWidget {
  TournamentList({Key key}) : super(key: key);

  _TournamentListState createState() => _TournamentListState();
}

class _TournamentListState extends State<TournamentList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance.collection('tournaments').snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) return new Text('Error: ${snapshot.error}');
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return new Text('Loading...');
            default:
              return new ListView(
                children:
                    snapshot.data.documents.map((DocumentSnapshot document) {
                  return _buildCard(document);
                }).toList(),
              );
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          print("new tournament created");
          this.onCreate(context);
        },
      ),
    );
  }

  void onCreate(BuildContext context) {
//    Navigator.of(context)
//        .push(new MaterialPageRoute(builder: (BuildContext context) {
//      return new TournamentForm();
//    }));
  createTournamnet();
  }

  void onTournamentClick(DocumentSnapshot tournament) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => Matches(tournament: tournament)));
  }

  Widget _buildCard(DocumentSnapshot tournament) {
    return new GestureDetector(
      onTap: () => {this.onTournamentClick(tournament)},
      child: Padding(
        child: TournamentCard(
          title: tournament["name"],
          description: tournament["description"],
          playersCount: tournament["playersCount"],
        ),
        padding: EdgeInsets.only(top: 15, left: 10, right: 10),
      ),
    );
  }

  createTournamnet() {
    Map<String, dynamic> getT() {
      Map<String, dynamic> test = new Map();
      test.putIfAbsent("description", () => "Tournament2 desc");
      test.putIfAbsent("name", () => "Tournament2");
      test.putIfAbsent("playersCount", () => 5);

      // Match1
      Map<String, dynamic> match1 = new Map();
      match1.putIfAbsent("matchId", () => "MatchIdnew");
      match1.putIfAbsent("teams", () =>
      [
        {
          "score": -1,
          "teamId": "Team5"
        },
        {
          "score": -1,
          "teamId": "Team6"
        }
      ]);
      Map<String, dynamic> match2 = new Map();
      match2.putIfAbsent("matchId", () => "MatchIdnew2");
      match2.putIfAbsent("teams", () =>
      [
        {
          "score": -1,
          "teamId": "Team7"
        },
        {
          "score": -1,
          "teamId": "Team8"
        }
      ]);
      test.putIfAbsent("matches", () => [match1, match2]);
      test.putIfAbsent("teams", () =>
      [{
        "id": "team7",
        "name": "Team7",
        "players": ["P1", "P2"]
      },
        {
          "id": "team8",
          "name": "Team8",
          "players": ["P3", "P4"]
        }
      ]);
      return test;
    }
    print(getT());
    Firestore.instance.collection('tournaments').document().setData(getT());
  }
}

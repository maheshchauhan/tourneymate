import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:tourneymate/util/strings.dart';

class RecordScore extends StatefulWidget {
  final String teamA;
  final String teamB;
  final String tournamentId;
  final String matchId;

  const RecordScore(
      {Key key, this.teamA, this.teamB, this.tournamentId, this.matchId})
      : super(key: key);

  @override
  _RecordScoreState createState() => _RecordScoreState();
}

class _RecordScoreState extends State<RecordScore> {
  TextEditingController teamAScoreController = new TextEditingController();
  TextEditingController teamBScoreController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Strings.appName),
      ),
      body: Center(
          child: Padding(
        padding: EdgeInsets.all(20.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextFormField(
              controller: teamAScoreController,
              decoration: InputDecoration(
                  hintText: 'Enter ${widget.teamA} score',
                  labelText: 'Enter ${widget.teamA} score'),
            ),
            TextFormField(
              controller: teamBScoreController,
              decoration: InputDecoration(
                  hintText: 'Enter ${widget.teamB} score',
                  labelText: 'Enter ${widget.teamB} score'),
            ),
            ButtonTheme.bar(
              // make buttons use the appropriate styles for cards
              child: ButtonBar(
                children: <Widget>[
                  RaisedButton(
                    child: const Text('Save'),
                    textColor: Colors.black,
                    onPressed: () {
                      saveScore(int.parse(teamAScoreController.text),
                          int.parse(teamBScoreController.text));
                    },
                  ),
                ],
              ),
            )
          ],
        ),
      )),
    );
  }

  saveScore(int teamAScore, int teamBScore) {
    DocumentReference docRef =
        Firestore.instance.document('tournaments/' + widget.tournamentId);

    //.where("name", isEqualTo: "test")
    Map<String, dynamic> values;
    docRef.snapshots().forEach((DocumentSnapshot value) => {
          values = value.data,
          value.data["matches"].forEach((data) => {
                if (data["matchId"] == widget.matchId)
                  {
                    print(data["teams"][0]["score"]),
                    data["teams"][0]["score"] = teamAScore,
                    data["teams"][1]["score"] = teamBScore,
                    Navigator.pop(context)
                  },
              docRef.updateData(values)
              })
        });
  }
}

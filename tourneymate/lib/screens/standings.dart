import 'package:flutter/material.dart';
import 'package:tourneymate/widgets/StandingTile.dart';

class Standings extends StatefulWidget {
  Standings({
    Key key,
  }) : super(key: key);

  _StandingsState createState() => _StandingsState();
}

class _StandingsState extends State<Standings> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
          color: Theme.of(context).scaffoldBackgroundColor,
          child: Column(children: <Widget>[
            _buildTitleCard("Tournament Name", "Description", 2),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                child: _buildStandingsTable(),
              ),
            )
          ])),
    );
  }

  Widget _buildStandingsTable() => Material(
      child: DataTable(
          sortColumnIndex: 2,
          sortAscending: true,
          columns: <DataColumn>[
            DataColumn(
                label: Text("Team"),
                numeric: false,
                onSort: (index, isAscending) {},
                tooltip: "Team Standings"),
            DataColumn(
                label: Text("GD"), numeric: true, tooltip: "Goal Difference"),
            DataColumn(
                label: Text("Pts"),
                numeric: true,
                tooltip: "Points accrued by each team"),
          ],
          rows: standings
              .map((standing) => DataRow(cells: [
                    DataCell(Row(children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Image.asset(
                          "assets/foosball.png",
                          width: 15,
                          height: 15,
                        ),
                      ),
                      SizedBox(width: 10),
                      Center(child: Text(standing.team))
                    ])),
                    DataCell(Text(standing.goalDifference.toString())),
                    DataCell(Text(standing.points.toString())),
                  ]))
              .toList()));

  Widget _buildTitleCard(String title, String description, int playersCount) {
    return Padding(
      child: StandingTile(
        title: title,
        playersCount: playersCount,
      ),
      padding: EdgeInsets.only(top: 20),
    );
  }
}

class StandingRecord {
  String team;
  String icon;
  int goalDifference;
  int points;
  StandingRecord({this.team, this.icon, this.goalDifference, this.points});
}

final standings = <StandingRecord>[
  StandingRecord(
      team: "Aj", icon: "/assets/icon", goalDifference: 40, points: 100),
  StandingRecord(
      team: "AB", icon: "/assets/icon", goalDifference: 1, points: 1),
  StandingRecord(
      team: "A", icon: "/assets/icon", goalDifference: -1, points: 1),
  StandingRecord(team: "A", icon: "/assets/icon", goalDifference: 1, points: 1),
  StandingRecord(team: "A", icon: "/assets/icon", goalDifference: 1, points: 1),
  StandingRecord(team: "A", icon: "/assets/icon", goalDifference: 1, points: 1),
  StandingRecord(team: "A", icon: "/assets/icon", goalDifference: 1, points: 1),
  StandingRecord(team: "A", icon: "/assets/icon", goalDifference: 1, points: 1),
  StandingRecord(team: "A", icon: "/assets/icon", goalDifference: 1, points: 1),
  StandingRecord(team: "A", icon: "/assets/icon", goalDifference: 1, points: 1),
  StandingRecord(team: "A", icon: "/assets/icon", goalDifference: 1, points: 1),
];

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:tourneymate/screens/match_form.dart';
import 'package:tourneymate/util/strings.dart';
import 'package:tourneymate/widgets/MatchTile.dart';

class Matches extends StatefulWidget {

  Matches({Key key, this.tournament}) : super(key: key);
  final DocumentSnapshot tournament;

  @override
  State<StatefulWidget> createState() {
    return _MatchesState();
  }
}

class _MatchesState extends State<Matches> {
  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> tournament = Map<String, dynamic>.from ( widget.tournament.data );
    List<dynamic> matches = tournament["matches"].toList();
    return Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text(Strings.appName),
        ),
        body: ListView(
          padding: EdgeInsets.all(5.0),
          children: matches.map((dynamic) {
            return buildMatch(dynamic, tournament["name"], dynamic["matchId"]);
          }).toList(),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            this.onCreate(context);
          },
        ));
  }

  Widget buildMatch(dynamic, String tId, String matchId) {
    return MatchTile(teamA: dynamic["teams"][0]["teamId"],
      teamAScore: dynamic["teams"][0]["score"],
      teamB: dynamic["teams"][1]["teamId"],
      teamBScore: dynamic["teams"][1]["score"], tournamentId: widget.tournament.documentID, matchId: matchId);
  }

  void onCreate(BuildContext context) {
    Navigator.of(context)
        .push(new MaterialPageRoute(builder: (BuildContext context) {
      return new MatchForm();
    }));
  }
}

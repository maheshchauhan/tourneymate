import 'package:flutter/material.dart';
import 'package:tourneymate/screens/login_page.dart';
import 'package:tourneymate/util/strings.dart';

class App extends StatelessWidget {
  const App({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: Strings.appName,
      theme: _buildTheme(ThemeData.dark()),
      routes: {
        "/": (context) {
          return LoginPage();
        }
      },
    );
  }

  _buildTheme(ThemeData baseTheme) {
    return baseTheme.copyWith(
        // primaryColor: Colors.redAccent,
        backgroundColor: Colors.red,
        primaryTextTheme: _getTextTheme(baseTheme.primaryTextTheme),
        textTheme: _getTextTheme(baseTheme.textTheme),
        primaryIconTheme: _getIconTheme(baseTheme.primaryIconTheme),
        scaffoldBackgroundColor: Color(0XFF102334),
        accentColor: Color(0XFFc66247),
        primaryColor: Color(0XFF303748),
        canvasColor: Color(0XAA141414),
        cardColor: Color(0XFF303748),
        iconTheme: _getIconTheme(baseTheme.iconTheme),
        floatingActionButtonTheme:
            _getFloatingButtonTheme(baseTheme.floatingActionButtonTheme));
  }

  _getTextTheme(TextTheme baseTheme) {
    return baseTheme.copyWith(
        body1: baseTheme.body2.copyWith(fontSize: 12),
        body2: baseTheme.body2.copyWith(fontSize: 10),
        caption: baseTheme.caption
            .copyWith(fontSize: 20, decoration: TextDecoration.none),
        subtitle: baseTheme.subtitle.copyWith(fontSize: 18));
  }

  _getIconTheme(IconThemeData base) {
    return base.copyWith(size: 30, opacity: 1);
  }

  _getFloatingButtonTheme(FloatingActionButtonThemeData baseTheme) {
    return baseTheme.copyWith(backgroundColor: Color(0XFFc66247));
  }
}

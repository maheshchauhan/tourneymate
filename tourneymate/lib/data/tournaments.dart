class Tournaments {
  static List<Tournament> tournaments = <Tournament>[
    Tournament(
        "Pro Fifa League 2018",
        "This is the greatest tournament in the world.Only winners allowed. s",
        2),
    Tournament(
        "Pro Fifa League 2019",
        "This is the greatest tournament in the world.Only winners allowed.",
        3),
    Tournament("Pro Fifa League 2020",
        "This is the greatest tournament in the world.Only winners allowed.", 4)
  ];
}

class Tournament {
  final String title;
  final String description;
  final int playersCount;

  Tournament(this.title, this.description, this.playersCount);
}
